package app01.source;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app01.model.User;
import app01.service.UserService;

/**
 * APIController
 */
@RestController
@RequestMapping("rest/")
public class APIController {

  @Autowired
  private UserService userService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @GetMapping("test1")
  public String test1() {
    return "<h1>Test1</h1>";
  }

  @GetMapping("test2")
  public String test2() {
    return "<h1>Test2</h1>";
  }

  @GetMapping("users")
  public List<User> users() {
    return userService.getAllUser();
  }

  @PostMapping("add")
  public User addNewUser(@RequestBody User user) {
    User data = new User();

    String rawPass = user.getPassword();
    String password = passwordEncoder.encode(rawPass);

    data.setUsername(user.getUsername());
    data.setPassword(password);
    data.setActive(1);
    data.setPermissions(user.getPermissions());
    data.setRoles(user.getRoles());
    return userService.saveUser(data);
  }

}