package app01.service;

import java.util.List;

import app01.model.User;

/**
 * UserService
 */
public interface UserService {

  List<User> getAllUser();

  User saveUser(User user);

}