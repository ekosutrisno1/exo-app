package app01.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import app01.model.User;

/**
 * UserPrincipal
 */
public class UserPrincipal implements UserDetails {

  private static final long serialVersionUID = 8219874187914333760L;
  private User user;

  public UserPrincipal(User user) {
    this.user = user;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    List<GrantedAuthority> authorities = new ArrayList<>();

    // mengambil list nama Permission dari User
    this.user.getPermissionList().forEach(perm -> {
      GrantedAuthority authority = new SimpleGrantedAuthority(perm);
      authorities.add(authority);
    });

    // mengambil list nama ROLE_NAME dari User
    this.user.getRoleList().forEach(role -> {
      GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + role);
      authorities.add(authority);
    });

    return authorities;
  }

  @Override
  public String getPassword() {
    return this.user.getPassword();
  }

  @Override
  public String getUsername() {
    return this.user.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return this.user.getActive() == 1;
  }

}