package app01.service.SerImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app01.database.UserRepository;
import app01.model.User;
import app01.service.UserService;

/**
 * UserServiceImpl
 */
@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepo;

  @Override
  public List<User> getAllUser() {
    return userRepo.findAll();
  }

  @Override
  public User saveUser(User user) {
    return userRepo.save(user);
  }

}