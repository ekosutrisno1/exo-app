package app01.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import app01.database.UserRepository;
import app01.model.User;

/**
 * UserPrincipalDetailsService
 */
@Service
public class UserPrincipalDetailsService implements UserDetailsService {

  @Autowired
  private UserRepository userRepo;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepo.findByUsername(username);
    UserPrincipal userPrincipal = new UserPrincipal(user);

    return userPrincipal;
  }

}