package app01.security;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import app01.service.UserPrincipalDetailsService;

/**
 * SecurityConfiguration
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserPrincipalDetailsService userDetailsService;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(authenticationProvider());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http.authorizeRequests()

        .antMatchers("/home", "/register").permitAll()

        .antMatchers("/app/profile/**").authenticated()

        .antMatchers("/app/admin/**").hasRole("ADMIN")

        .antMatchers("/app/management/**").hasAnyRole("ADMIN", "MANAGER")

        .antMatchers("/rest/**").hasAnyAuthority("ACCES_TEST1", "ACCES_TEST2")

        .and().formLogin().loginProcessingUrl("/signin")

        .loginPage("/login").permitAll()

        .usernameParameter("txtUsername").passwordParameter("txtPassword")

        .and().rememberMe().tokenValiditySeconds((int) TimeUnit.DAYS.toSeconds(2)).key("ekosutrisno")

        .rememberMeParameter("checkRememberMe")

        .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")

        .clearAuthentication(true).invalidateHttpSession(true)

        .deleteCookies("JSESSIONID", "remember-me").logoutSuccessUrl("/login")

    ;
  }

  @Bean
  DaoAuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
    daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
    daoAuthenticationProvider.setUserDetailsService(userDetailsService);
    return daoAuthenticationProvider;
  }

  @Bean
  PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}