package app01.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * ErrorController
 */
@Controller
public class MyErrorController implements ErrorController {

  @RequestMapping("/error")
  public ModelAndView handleError(HttpServletRequest request) {
    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
    ModelAndView errorPage = new ModelAndView("error/errorPage");
    String errorMsg = "";
    String kode = "";
    String pesanPanjang = "";
    String urlgambar = "";

    if (status != null) {
      Integer statusCode = Integer.valueOf(status.toString());

      switch (statusCode) {
      case 400: {
        kode = Integer.toString(statusCode);
        urlgambar = "/img/400.svg";
        errorMsg = "Bad Request";
        pesanPanjang = "Pastikan penulisan alamat url benar. Silahkan Cek terlebih dahulu.";
        break;
      }
      case 401: {
        kode = Integer.toString(statusCode);
        urlgambar = "/img/401.svg";
        errorMsg = "Unauthorized";
        pesanPanjang = "Sepertinya kamu belum login atau belum registrasi akun, yuk registrasi dulu baru nanti kesini lagi.";
        break;
      }
      case 403: {
        kode = Integer.toString(statusCode);
        urlgambar = "/img/403.svg";
        errorMsg = "Acces Ditolak";
        pesanPanjang = "Maaf, anda tidak dapat mengakses halaman ini, karena anda tidak memiliki otoritas untuk mengakses."
            + " silahkan kembali kehalaman sebelumnya. thanks..";
        break;
      }
      case 404: {
        kode = Integer.toString(statusCode);
        urlgambar = "/img/404.svg";
        errorMsg = "Resource not found";
        pesanPanjang = "Wahhhh.. sepertinya halaman yang kamu tuju tidak ada, pilih halaman yang lain aja ya.";
        break;
      }
      case 500: {
        kode = Integer.toString(statusCode);
        urlgambar = "/img/500.svg";
        errorMsg = "Internal Server Error";
        pesanPanjang = "Sereeemmmmm... Sepertinya ada kesalahan pada bagian server. segera cek atau laporkan kepada pengembang.";
        break;
      }
      }
    }
    errorPage.addObject("errorMsg", errorMsg);
    errorPage.addObject("kode", kode);
    errorPage.addObject("urlgambar", urlgambar);
    errorPage.addObject("pesanPanjang", pesanPanjang);
    return errorPage;

  }

  @Override
  public String getErrorPath() {
    return "/error";
  }
}