package app01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ApplicationController
 */
@Controller
@RequestMapping("/")
public class ApplicationController {

  @GetMapping("home")
  public String indexPage() {
    return "index";
  }

  @GetMapping("login")
  public String loginPage() {
    return "auth/login";
  }

  @GetMapping("register")
  public String registerPage() {
    return "auth/register";
  }

  @GetMapping("app/profile")
  public String profilePage() {
    return "view/profile";
  }

  @GetMapping("app/admin")
  public String adminPage() {
    return "view/admin";
  }

  @GetMapping("app/management")
  public String managementPage() {
    return "view/management";
  }
}